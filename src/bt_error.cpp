#include "bt_error.hpp"
using namespace bt;

const char* bt::error::get_infostring(types& type)
{
	switch (type)
	{
	case types::lua_init: return "the scripting runtime could not initialize itself";
	case types::lua_load: return "the scripting runtime could not load an important file";
	case types::lua_deinit: return "the scripting runtime could not destroy itself";
	case types::obj_full: return "the object factory ran out of memory";
	}
}

bt::error::error(types&& type) : _type{ type }, std::runtime_error{ get_infostring(_type) } {
}

