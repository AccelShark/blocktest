#include "bt.hpp"
#include "bt_lua.hpp"
int main(int argc, char *argv[]) {
	// Gimme everything you got
	std::printf("Blocktest %s %s\n", BLOCKTEST_VERSION_STRING, BLOCKTEST_REVISION_STRING);
	assert(SDL_Init(SDL_INIT_EVERYTHING) == 0);
	std::printf("... SDL\n");

	// Do core GL4.5 attributes
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	std::printf("... SDL GL\n");

	// Make a window
	auto *w = SDL_CreateWindow("Blocktest", SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_OPENGL);
	std::printf("... SDL window\n");
	assert(w != nullptr);

	// Context for window
	auto w_context = SDL_GL_CreateContext(w);
	std::printf("... SDL GL window\n");

	// GLEW window
	glewExperimental = GL_TRUE;
	auto glew = glewInit();
	std::printf("... GLEW\n");
	assert(glew == GLEW_OK);


	bt::lua::init(std::filesystem::path{ SDL_GetPrefPath("AccelShark", "Blocktest") });
	SDL_Delay(3000);
	bt::lua::deinit();

	std::printf("Tear down\n");
	SDL_GL_DeleteContext(w_context);
	SDL_DestroyWindow(w);
	SDL_Quit();
	return EXIT_SUCCESS;
}