#include "bt_error.hpp"
#include "bt_lua.hpp"
using namespace bt;

static auto lua_path = std::unique_ptr<std::filesystem::path>{nullptr};
static lua_State *L = nullptr;
static auto has_init = false;
static auto btree = std::unique_ptr<lua::btree_split>{ nullptr };
static constexpr auto btree_limit = 4;
#if 0
template<U8 I> void lua::btree_branch(lua_State* L, U8 i) {
	if (i > I) {
		lua_createtable(L, 2, 0);
		lua_pushboolean(L, false);
		lua_rawseti(L, -2, 1);
		lua_pushboolean(L, false);
		lua_rawseti(L, -2, 2);
	}
	else {
		lua_createtable(L, 2, 0);
		btree_branch<I>(L, i + 1);
		lua_rawseti(L, -3, 2);
		btree_branch<I>(L, i + 1);
		lua_rawseti(L, -3, 2);
	}
}
#endif
bool lua::init(std::filesystem::path&& _path) {
	if (!has_init) {
		lua_path = std::make_unique<std::filesystem::path>(_path);
		std::printf("... [LUA] constructing a btree\n");
		btree = std::make_unique<lua::btree_split>(btree_limit);
		std::printf("... [LUA] btree has been made\n");

		for(auto i = 0; i < 32; i++) {
			obj_create("test", otypes::lua_object);
		}
		L = luaL_newstate();
		std::printf("... [LUA] state\n");
		luaL_openlibs(L);
		std::printf("... [LUA] libs\n");

		// Create the Blocktest table, then create the B-tree
		lua_createtable(L, 0, 4);
		lua_pushnumber(L, BLOCKTEST_VERSION_MAJOR);
		lua_setfield(L, -2, "major");
		lua_pushnumber(L, BLOCKTEST_VERSION_MINOR);
		lua_setfield(L, -2, "minor");
		lua_pushnumber(L, BLOCKTEST_VERSION_PATCH);
		lua_setfield(L, -2, "patch");
		lua_pushnumber(L, BLOCKTEST_REVISION);
		lua_setfield(L, -2, "rev");

		lua_setglobal(L, "blocktest");

		if(!load_script(*lua_path / "start.lua"))
			throw error{ error::types::lua_load };

		std::printf("... [LUA] loaded start file\n");

		// We can come to the conclusion that we are okay now.
		has_init = true;
		return has_init;
	}
	throw error{ error::types::lua_init };
}

bool lua::load_script(std::filesystem::path&& _path) {
	auto lua_error = luaL_loadfile(L, _path.string().c_str());
	if (lua_error) {
		std::fprintf(stderr, "... [LUA] loadfile: %s\n", lua_tostring(L, -1));
		lua_pop(L, 1);
		return false;
	}
	lua_error = lua_pcall(L, 0, 0, 0);
	if (lua_error) {
		std::fprintf(stderr, "... [LUA] pcall: %s\n", lua_tostring(L, -1));
		lua_pop(L, 1);
		return false;
	}

	return true;
}
void lua::deinit() {
	if (has_init) {
		std::printf("... [LUA] tear down\n");
		lua_close(L);
		lua_path = nullptr;
		has_init = false;
		return;
	}
	throw error{ error::types::lua_deinit };
}
#if 0
int bt::lua::object::get_type(lua_State*)
{
	int n = lua_gettop(L);
	if (n != 0) {
		lua_pushstring(L, "incorrect number of arguments");
		lua_error(L);
		return 0;
	}
	lua_pushstring(L, "LuaObject");
	return 1;
}
int lua::object::index(lua_State* L) {
	int n = lua_gettop(L);
	if (n != 1) {
		lua_pushstring(L, "incorrect number of arguments");
		lua_error(L);
		return 0;
	}
	if (!lua_isnumber(L, 1)) {
		lua_pushstring(L, "incorrect argument");
		lua_error(L);
		return 0;
	}
	std::bitset<btree_limit> btree_index{ lua_tonumber(L, -1) };
	auto what = btree.get();
	for (auto i = btree_limit; i > 0; i--) {
		if (btree_index[i]) {
			if (!what->f2) {
				// no value in btree, return nil
				lua_pushnil(L);
				return 1;
			}

			what = static_cast<btree_split*>(what->o2);
		}
		else {
			if (!what->f1) {
				lua_pushnil(L);
				return 1;
			}

			what = static_cast<btree_split*>(what->o1);
		}
	}
	if (btree_index[0]) {
		if (!what->f2) {
			// no value in btree, return nil
			lua_pushnil(L);
			return 1;
		}
		auto obj = static_cast<btree_node*>(what->o2)->obj;
		lua_createtable(L, 0, 0);
		// TODO
		return 1;
	}
	else {
		if (!what->f1) {
			// no value in btree, return nil
			lua_pushnil(L);
			return 1;
		}
		auto obj = static_cast<btree_node*>(what->o1)->obj;
		lua_createtable(L, 0, 0);
		return 1;
	}
}
#endif

lua::ntypes lua::btree_split::get_type() const {
	return lua::ntypes::split;
}

lua::ntypes lua::btree_node::get_type() const {
	return lua::ntypes::node;
}

U32 lua::obj_create(const char* name, const otypes otype) {
	std::bitset<btree_limit> btree_result{ 0x0 };
	auto c = btree.get();
	for (auto i = 0; i < btree_limit; i++) {
		auto index = btree_limit - i - 1;
		if (c->f1_empty) {
			btree_result[index] = false;
			assert(c->get_type() == ntypes::split);
			if (!index) {
				c->f1_empty = false;
			}
			c = dynamic_cast<btree_split*>(c->o1);
		} 
		else if (c->f2_empty) {
			btree_result[index] = true;
			assert(c->get_type() == ntypes::split);
			if (!index) {
				c->f2_empty = false;
			}
			c = dynamic_cast<btree_split*>(c->o2);
		}
		else {
			while (++index < btree_limit) {
				if (btree_result[index]) {
					c->parent->f2_empty = false;
				}
				else {
					c->parent->f1_empty = false;
				}
			}
			assert(false);
		}
	}
	std::printf("%lu\n", btree_result.to_ulong());
	return btree_result.to_ulong();
}
#if 0
U32 lua::obj_create(const char* name, const otypes otype)
{
	std::bitset<btree_limit> btree_result{ 0x0 };
	// set values
	auto cursor = static_cast<btree_base*>(btree.get());
	auto here = dynamic_cast<btree_split*>(cursor);
	auto c1 = dynamic_cast<btree_split*>(here->o1);
	auto c2 = dynamic_cast<btree_split*>(here->o2);

	// check
	assert(cursor->get_type() != ntypes::node);

	// seed tree lookup
	auto i = btree_limit - 1;
	if (here->f1 && !here->f2) {
		cursor = c2;
		btree_result[i] = true;
	}
	else if(!here->f2) {
		cursor = c1;
		btree_result[i] = false;
	}
	else {
		throw error{ error::types::obj_full };
	}

	i--;

	while(i) {
		assert(cursor->get_type() != ntypes::node);
		here = dynamic_cast<btree_split*>(cursor);
		c1 = dynamic_cast<btree_split*>(here->o1);
		c2 = dynamic_cast<btree_split*>(here->o2);

		if (btree_result[i]) {
			here->f2 = true;
			cursor = c2;
			btree_result[i] = true;
		}
		else {
			here->f1 = true;
			cursor = c1;
			btree_result[i] = false;
		}
		i--;
	}
	here = dynamic_cast<btree_split*>(cursor);
	if (btree_result[0]) {
		here->f2 = true;
	}
	else {
		here->f1 = true;
	}
	std::printf("... [LUA] object created %lu\n", btree_result.to_ulong());
	while (i != btree_limit) {
		assert(cursor != nullptr);
		here = dynamic_cast<btree_split*>(cursor);
		if (btree_result[i]) {
			if (here->is_full()) {
				here->parent->f2 = true;
			}
		} else {
			if (here->is_full()) {
				here->parent->f1 = true;
			}
		}
		i++;
	}
	return btree_result.to_ulong();
}
lua::object* lua::obj_index(const U32 idx) {
	std::bitset<btree_limit> btree_index{ idx };
	auto what = btree.get();
	for (auto i = btree_limit; i > 0; i--) {
		if (btree_index[i]) {
			if (what->f2) {
				return nullptr;
			}

			what = static_cast<btree_split*>(what->o2);
		}
		else {
			if (!what->f1) {
				return nullptr;
			}

			what = static_cast<btree_split*>(what->o1);
		}
	}
	if (btree_index[0]) {
		return static_cast<btree_node*>(what->o2)->obj;
	}
	else {
		if (!what->f1) {
			// no value in btree, return nil
			return nullptr;
		}
		return static_cast<btree_node*>(what->o1)->obj;
	}
}
#endif

lua::btree_split::btree_split(const U8 depth_left) :o1{ nullptr }, o2{ nullptr }
{
	if (depth_left > 0) {
		auto depth = depth_left;
		depth--;
		o1 = new lua::btree_split{ depth };
		o1->parent = this;
		o2 = new lua::btree_split{ depth };
		o2->parent = this;
		return;
	}
	o1 = new lua::btree_node;
	o1->parent = this;
	o2 = new lua::btree_node;
	o2->parent = this;
}

lua::btree_split::~btree_split() {
	delete o1;
	delete o2;
}