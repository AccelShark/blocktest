#pragma once
#include "bt.hpp"

namespace bt {
	class error : std::runtime_error {
	public:
		enum class types
		{
			lua_init,
			lua_load,
			lua_deinit,
			obj_full
		};
	private:
		types _type;
	public:
		const static char *get_infostring(types&);
		error(types&&);
	};
}