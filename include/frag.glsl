#version 450 core
in vec4 frag_paint;
out vec4 color;
void main(){
    // Mix
    if (frag_paint.a < 0.001f) { discard; }
    color = vec4(mix(frag_paint.rgb, back.rgb, clamp(frag_paint.a, 0.0f, 1.0f)), 1.0f);
}
