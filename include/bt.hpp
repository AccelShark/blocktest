#pragma once
#include "bt_config.hpp"
#include <GL/glew.h>
#include <lua.hpp>
#if defined(_WIN32) || defined(_WIN64)
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <bitset>
#include <chrono>
#include <exception>
#include <filesystem>
#include <memory>
#include <mutex>
#include <unordered_map>
#include <stdexcept>
#include <thread>

using U8 = std::uint8_t;
using S8 = std::int8_t;
using U16 = std::uint16_t;
using S16 = std::int16_t;
using U32 = std::uint32_t;
using S32 = std::int32_t;
using U64 = std::uint64_t;
using S64 = std::int64_t;
using F32 = float;
using F64 = double;