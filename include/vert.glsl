#version 450 core
const float PI = 3.1415926535897932384626433832795;
in vec4 axis;
in vec4 angle;

const mat4x4 ident = mat4x4(
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f);

mat4x4 rotate(vec4 angle) {
    vec4 vert_n = vec4(normalize(angle.xyz), 1.0f);
    float vert_rotate = mod(angle.w, PI * 2.0f);
    // Antisymmetric Matrix
    mat4x4 anti = mat4x4(
    0.0f, -vert_n.z, vert_n.y, 0.0f,
    vert_n.z, 0.0f, -vert_n.x, 0.0f,
    -vert_n.y, vert_n.x, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f);
    return ident + (sin(vert_rotate) * anti) + ((1.0f - cos(vert_rotate)) * (anti * anti));
}
void main() {
    gl_Position = vec4(axis.xy, 0.0f, 1.0f);
}