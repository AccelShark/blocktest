#pragma once
#include "bt.hpp"

namespace bt {
	namespace lua {
		bool init(std::filesystem::path&&);
		bool load_script(std::filesystem::path&&);
		// template<U8> void btree_branch(lua_State*, U8);
		void deinit();

		enum class ntypes {
			split,
			node,
		};

		enum class otypes {
			lua_object,
		};

		class object {
			constexpr static auto _otype = otypes::lua_object;
		public:
			static int get_type(lua_State*);
			static int index(lua_State*);
		};
		struct btree_split;
		struct btree_base {
			btree_split* parent{nullptr};
			virtual ntypes get_type() const = 0;
		};
		struct btree_split : btree_base {
			ntypes get_type() const;

			// Do we have something inside?
			bool f1_empty = true;
			bool f2_empty = true;
			// Objects
			btree_base* o1;
			btree_base* o2;

			btree_split(const U8);
			~btree_split();
		};
		struct btree_node : btree_base {
			ntypes get_type() const;
			object* obj = nullptr;
		};
		U32 obj_create(const char*, const otypes);
		// object* obj_index(const U32);
		void obj_remove(const U32);
	}
}